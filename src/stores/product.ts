import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const dialog = ref(false);
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: "", price: 0 });

  // clear dialog when close dialog(dialog == false)
  watch(dialog, (newDialog, oldDialog) => {
    // console.log(newDialog);
    if (!newDialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });
  // push product from server
  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }
  async function saveProducts() {
    // console.log(editedProduct.value);
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูลได้");
    }
    await getProducts();
    dialog.value = false;
  }

  async function deleteProduct(id: number) {
    try {
      const res = await productService.deleteProduct(id);
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูลได้");
    }
    await getProducts();
  }

  function editProduct(product: Product) {
    //for not show is editing -JSON

    dialog.value = true;
    editedProduct.value = JSON.parse(JSON.stringify(product));
  }
  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProducts,
    editProduct,
    deleteProduct,
  };
});
